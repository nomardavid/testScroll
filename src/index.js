//flags
let lastScrollPosition = 0;
let ticking = false;

//icon features Flags
let coverageFirstTime  = false;
let exclusionsFirsTime  = false;
let mechanicsFirstTime  = false;
let costFirstTime  = false;
let optionalFirstTime  = false;
let ratingsFirstTime  = false;

//icon studies Flags
let experienceFirstTime  = false;
let conditionsFirstTime  = false;
let instructionFirstTime  = false;
let exampleFirsTime  = false;
let relaxFirstTime  = false;
let discountsFirstTime  = false;


// features elements icons
const coverageIcon = document.getElementById('coverageIcon');
const exclusionsIcon = document.getElementById('exclusionsIcon');
const mechanicsIcon = document.getElementById('mechanicsIcon');
const costIcon = document.getElementById('costIcon');
const optionalIcon = document.getElementById('optionalIcon');
const ratingsIcon = document.getElementById('ratingsIcon');

// features elements icons
const experienceIcon = document.getElementById('experienceIcon');
const conditionsIcon = document.getElementById('conditionsIcon');
const instructionIcon = document.getElementById('instructionIcon');
const exampleIcon = document.getElementById('exampleIcon');
const relaxIcon = document.getElementById('relaxIcon');
const discountsIcon = document.getElementById('discountsIcon');

//navs referencies
const featuresNav = document.getElementById('navbarFeatures');
const studentsNav = document.getElementById('navbarStudents');
const studentSection = document.getElementById('guide-student-drivers');

//title elements features
const coverageTitle = document.getElementById('coverage');
const exclusionTitle = document.getElementById('exclusions');
const mechanicsTitle = document.getElementById('mechanics');
const costTitle = document.getElementById('cost');
const optionalTitle = document.getElementById('optional');
const ratingsTitle = document.getElementById('ratings');

//title elements students
const experienceTitle = document.getElementById('experience');
const conditionsTitle = document.getElementById('conditions');
const intructionTitle = document.getElementById('instruction');
const exampleTitle = document.getElementById('example');
const relaxTitle = document.getElementById('relax');
const discountsTitle = document.getElementById('discounts');

//scrolls id's
const scrollerFeatures = document.getElementById('scrollerFeatures');
const scrollerStudents = document.getElementById('scrollerStudents');

//start div body students
const guideAudenciePanel = document.getElementById('guide-audience-panel');

function getOffsetTop( elem ) {
    const offsetTop = elem.offsetTop;
    return offsetTop;
}

function getXPosition( elem) {
  const left = elem.offsetLeft;
  return left;
}

function showStudentNav(scroll_pos) {
    const coveragePos = getOffsetTop(coverageTitle);
    const exclusionsPos = getOffsetTop(exclusionTitle);
    const mechanicsPos = getOffsetTop(mechanicsTitle);
    const costPos = getOffsetTop(costTitle);
    const optionalPos = getOffsetTop(optionalTitle);
    const ratingsPos = getOffsetTop(ratingsTitle);

    const guideAudenciePanelPos = getOffsetTop(guideAudenciePanel);

    const experiencePos = getOffsetTop(experienceTitle) + guideAudenciePanelPos;
    const conditionsPos = getOffsetTop(conditionsTitle) + guideAudenciePanelPos;
    const instructionPos = getOffsetTop(intructionTitle) + guideAudenciePanelPos;
    const examplePos = getOffsetTop(exampleTitle) + guideAudenciePanelPos;
    const relaxPos = getOffsetTop(relaxTitle) + guideAudenciePanelPos;
    const discountsPos = getOffsetTop(discountsTitle) + guideAudenciePanelPos;

    const postMarginPixelsFeatures = 50;
    const postMarginStudents = 80;

    //coverage validations
    if(scroll_pos <= coveragePos) {
      coverageFirstTime = true;
      const x = getXPosition(coveragePos);
      scrollerFeatures.scrollLeft = x;
      coverageIcon.classList.add('paintBlue')
      coverageFirstTime ? exclusionsIcon.classList.remove('paintBlue') : null;
    }
    
    //exclusions validations
    if(scroll_pos >= coveragePos && scroll_pos <= exclusionsPos) {
      coverageIcon.classList.remove('paintBlue');
      const x = getXPosition(exclusionsIcon);
      scrollerFeatures.scrollLeft = x;
      exclusionsIcon.classList.add('paintBlue');
      mechanicsFirstTime ? mechanicsIcon.classList.remove('paintBlue') : null;
    }

    //mechanics validations
    if(scroll_pos >= exclusionsPos && scroll_pos <= mechanicsPos) {
      mechanicsFirstTime = true;
      const x = getXPosition(mechanicsIcon);
      scrollerFeatures.scrollLeft = x;
      exclusionsIcon.classList.remove('paintBlue');
      mechanicsIcon.classList.add('paintBlue');
      costFirstTime ? costIcon.classList.remove('paintBlue') : null;
    }

    //cost validations
    if(scroll_pos >= mechanicsPos && scroll_pos <= costPos) {
      costFirstTime = true;
      const x = getXPosition(costIcon);
      scrollerFeatures.scrollLeft = x;
      mechanicsIcon.classList.remove('paintBlue');
      costIcon.classList.add('paintBlue');
      optionalFirstTime ? optionalIcon.classList.remove('paintBlue') : null;
    }

    //optionals validations
    if(scroll_pos >= costPos && scroll_pos <= optionalPos) {
      optionalFirstTime = true;
      const x = getXPosition(optionalIcon);
      featuresNav.scrollLeft = x;
      costIcon.classList.remove('paintBlue');
      optionalIcon.classList.add('paintBlue');
      optionalFirstTime ? ratingsIcon.classList.remove('paintBlue') : null;
    }

    //ratings validations
    if(scroll_pos >= optionalPos && scroll_pos <= ratingsPos) {
      ratingsFirstTime = true;
      const x = getXPosition(ratingsIcon);
      scrollerFeatures.scrollLeft = x;
      optionalIcon.classList.remove('paintBlue');
      ratingsIcon.classList.add('paintBlue');
      ratingsFirstTime ? experienceIcon.classList.remove('paintBlue') : null;
    }

    //experience validations
    if(scroll_pos >= guideAudenciePanelPos && scroll_pos <= experiencePos) {
      experienceFirstTime = true;
      const x = getXPosition(experienceIcon);
      scrollerStudents.scrollLeft = x;
      ratingsIcon.classList.remove('paintBlue');
      experienceIcon.classList.add('paintBlue');
      experienceFirstTime ? conditionsIcon.classList.remove('paintBlue') : null;
    }

    // //conditions validations
    if(scroll_pos >= experiencePos && scroll_pos <= conditionsPos) {
      conditionsFirstTime = true;
      const x = getXPosition(conditionsIcon);
      scrollerStudents.scrollLeft = x;
      experienceIcon.classList.remove('paintBlue');
      conditionsIcon.classList.add('paintBlue');
      conditionsFirstTime ? instructionIcon.classList.remove('paintBlue') : null;
    }

    //instruction validations
    if(scroll_pos >= conditionsPos && scroll_pos <= instructionPos) {
      instructionFirstTime = true;
      const x = getXPosition(instructionIcon);
      scrollerStudents.scrollLeft = x;
      conditionsIcon.classList.remove('paintBlue');
      instructionIcon.classList.add('paintBlue');
      instructionFirstTime ? exampleIcon.classList.remove('paintBlue') : null;
    }

    //example validations
    if(scroll_pos >= instructionPos && scroll_pos <= examplePos) {
      exampleFirsTime = true;
      const x = getXPosition(exampleIcon);
      scrollerStudents.scrollLeft = x;
      instructionIcon.classList.remove('paintBlue');
      exampleIcon.classList.add('paintBlue');
      exampleFirsTime ? relaxIcon.classList.remove('paintBlue') : null;
    }

    // //relax validations
    if(scroll_pos >= examplePos && scroll_pos <= relaxPos) {
      relaxFirstTime = true;
      const x = getXPosition(relaxIcon);
      scrollerStudents.scrollLeft = x;
      exampleIcon.classList.remove('paintBlue');
      relaxIcon.classList.add('paintBlue');
      relaxFirstTime ? discountsIcon.classList.remove('paintBlue') : null;
    }
    // //discounts validations
    if(scroll_pos >= relaxPos) {
      discountsFirstTime = true;
      const x = getXPosition(discountsIcon);
      scrollerStudents.scrollLeft = x;
      relaxIcon.classList.remove('paintBlue');
      discountsIcon.classList.add('paintBlue');
    }

    //hide feature navbar
    if(scroll_pos >= ratingsPos + postMarginPixelsFeatures) {
        featuresNav.classList.add('hideNavBar', 'absolute');
    }
    
    //show again feature navbar
    if(scroll_pos < ratingsPos) {
        featuresNav.classList.remove('hideNavBar', 'absolute');
        featuresNav.classList.add('showNavBar', 'fixed');
    }

    //show student navbar
    if(scroll_pos >= guideAudenciePanelPos - postMarginStudents) {
        const widthWindow = window.innerWidth;
        if(widthWindow >= 691) {
          studentSection.classList.add('post-margin-top');
        } //TODO: add another responsive post-margin-top
        studentsNav.classList.add('showNavBar', 'fixed');
    }

    //hide navbar
    if(scroll_pos <= guideAudenciePanelPos - postMarginStudents && scroll_pos >= ratingsPos) {
        studentSection.classList.remove('post-margin-top');
        studentsNav.classList.remove('showNavBar', 'fixed');
    }
    return;
}


window.addEventListener('scroll', function(e) {
  lastScrollPosition = window.scrollY;
  if (!ticking) {
    window.requestAnimationFrame(function() {
      showStudentNav(lastScrollPosition);
      ticking = false;
    });
  }
  ticking = true;
});
